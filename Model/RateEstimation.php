<?php
/**
 * Created by Rubikin Team.
 * Date: 11/13/13
 * Time: 3:25 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Model;


class RateEstimation
{
    protected $origin; # Location objects
    protected $destination;
    protected $packageRates; # array of hashes in the form of {:package => <Package>, :rate => 500}
    protected $carrier; # Carrier.name ('USPS', 'FedEx', etc.)
    protected $serviceName; # name of service ("First Class Ground", etc.)
    protected $serviceCode;
    protected $currency; # 'USD', 'CAD', etc.
    # http://en.wikipedia.org/wiki/ISO_4217
    protected $total;
    protected $shipping_date;
    protected $delivery_date; # Usually only available for express shipments
    protected $delivery_range; # Min and max delivery estimate in days
    protected $negotiated_rate;
    protected $insurance_price;

    public function __construct($origin, $destination, $carrier, $serviceName, array $options = array())
    {
        $this->origin = $origin;
        $this->destination = $destination;
        $this->carrier = $carrier;
        $this->serviceCode = $serviceName;
        $this->total = $options['total'];
        $this->packageRates = $options['rates'];
    }
}
