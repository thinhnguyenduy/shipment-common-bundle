<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 12:26 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Message;


use Nilead\ShipmentCommonComponent\Message\ResponseInterface;
use Nilead\ShipmentCommonComponent\Message\RequestInterface;

abstract class ResponseAbstract implements ResponseInterface
{
    protected $data;

    /**
     * @var RequestInterface
     */
    protected $request;

    public function __construct(RequestInterface $request, $data)
    {
        $this->request = $request;
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        return NULL;
    }
}
