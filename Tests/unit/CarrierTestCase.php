<?php
/**
 * Created by Rubikin Team.
 * Date: 4/22/14
 * Time: 2:48 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Tests\unit;

use Nilead\ShipmentCommonComponent\Carrier\CarrierInterface;

/**
 * Base Carrier Test class
 *
 * Ensures all carriers conform to consistent standards
 */
abstract class CarrierTestCase extends TestCase
{
    use \Codeception\Specify;

    /**
     * @var CarrierInterface
     */
    protected $carrier;

    public function testGetNameNotEmpty()
    {
        $name = $this->carrier->getName();
        $this->assertNotEmpty($name);
        $this->assertTrue(is_string($name));
    }

    public function testGetCodeNotEmpty()
    {
        $shortName = $this->carrier->getCode();
        $this->assertNotEmpty($shortName);
        $this->assertTrue(is_string($shortName));
    }

    public function testGetDefaultParametersReturnsArray()
    {
        $settings = $this->carrier->getDefaultParameters();
        $this->assertMemberType('array', $settings);
    }

    public function testDefaultParametersHaveMatchingMethods()
    {
        $settings = $this->carrier->getDefaultParameters();
        foreach ($settings as $key => $default) {
            $getter = 'get' . ucfirst($key);
            $setter = 'set' . ucfirst($key);
            $value = uniqid();

            $this->specify("Asserting that PerItemRateAdjuster returns the expected total", function ($getter) {
                $this->assertTrue(method_exists($this->carrier, $getter), "carrier must implement $getter()");
            });

            $this->assertTrue(method_exists($this->carrier, $setter), "carrier must implement $setter()");

            // setter must return instance
            $this->assertSame($this->carrier, $this->carrier->$setter($value));
            $this->assertSame($value, $this->carrier->$getter());
        }
    }

    public function testTestMode()
    {
        $this->assertSame($this->carrier, $this->carrier->setTestMode(false));
        $this->assertSame(false, $this->carrier->getTestMode());

        $this->assertSame($this->carrier, $this->carrier->setTestMode(true));
        $this->assertSame(true, $this->carrier->getTestMode());
    }

    public function testCurrency()
    {
        // currency is normalized to uppercase
        $this->assertSame($this->carrier, $this->carrier->setCurrency('eur'));
        $this->assertSame('EUR', $this->carrier->getCurrency());
    }

//    public function testSupportsVoid()
//    {
//        $supportsVoid = $this->carrier->supportsVoid();
//        $this->assertMemberType('boolean', $supportsVoid);
//
//        if ($supportsVoid) {
//            $this->assertInstanceOf('Nilead\ShipmentCommonComponent\Message\RequestInterface', $this->carrier->void());
//        } else {
//            $this->assertFalse(method_exists($this->carrier, 'void'));
//        }
//    }

//    public function testVoidParameters()
//    {
//        if ($this->carrier->supportsVoid()) {
//            foreach ($this->carrier->getDefaultParameters() as $key => $default) {
//                // set property on carrier
//                $getter = 'get' . ucfirst($key);
//                $setter = 'set' . ucfirst($key);
//                $value = uniqid();
//                $this->carrier->$setter($value);
//
//                // request should have matching property, with correct value
//                $request = $this->carrier->void();
//                $this->assertSame($value, $request->$getter());
//            }
//        }
//    }
}
